/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <gtest/gtest.h>

#include <prplprobe/deviceinfo.pb.h>
#include <prplprobe/common.pb.h>

#include "mock/mock_bus.h"

using namespace prplprobe::internal::v1;

extern EventList *getDeviceInfoEventList();

void populate_datamodel() {
    amxd_dm_t* dm = mock_get_dm();

    amxd_object_t *deviceinfo_obj = add_object_datamodel(amxd_dm_get_root(dm), "DeviceInfo");

    add_parameter_datamodel(deviceinfo_obj, "Manufacturer", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(deviceinfo_obj, "Manufacturer", "Sagemcom");

    add_parameter_datamodel(deviceinfo_obj, "ProductClass", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(deviceinfo_obj, "ProductClass", "Livebox 6");

    add_parameter_datamodel(deviceinfo_obj, "SerialNumber", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(deviceinfo_obj, "SerialNumber", "LK0123456789");

    add_parameter_datamodel(deviceinfo_obj, "SoftwareVersion", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(deviceinfo_obj, "SoftwareVersion", "1.2.3");

    amxd_object_t *netmodel_obj = add_object_datamodel(amxd_dm_get_root(dm), "NetModel");
    amxd_object_t *netmodel_intf_obj = add_object_datamodel(netmodel_obj, "Intf");
    amxd_object_t *netmodel_intf_ip_wan_obj = add_object_datamodel(netmodel_intf_obj, "ip-wan");

    add_parameter_datamodel(netmodel_intf_ip_wan_obj, "IPAddress", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(netmodel_intf_ip_wan_obj, "IPAddress", "10.168.1.50");

    amxd_object_t *netmodel_intf_eth_link_bridge_lan_obj = add_object_datamodel(netmodel_intf_obj, "ethLink-bridge_lan");

    add_parameter_datamodel(netmodel_intf_eth_link_bridge_lan_obj, "MACAddress", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(netmodel_intf_eth_link_bridge_lan_obj, "MACAddress", "01:23:45:67:89:AB");
}

// Basic test to check googletest works correctly
TEST(deviceinfo, getDeviceInfoEventList) {
    setup_amx_bus();
    populate_datamodel();

    EventList *event_list = getDeviceInfoEventList();
    EXPECT_EQ(event_list->event_size(), 1);

    Event event = event_list->event(0);
    EXPECT_EQ(event.kpiname(), "DeviceInfo");
    EXPECT_EQ(event.kpitype(), "");

    DeviceInfo info;
    event.event_data().UnpackTo(&info);

    EXPECT_EQ(info.cpe_type(), "Sagemcom Livebox 6");
    EXPECT_EQ(info.serial_number(), "LK0123456789");
    EXPECT_EQ(info.firmware(), "1.2.3");
    EXPECT_EQ(info.device_type(), "HGW");
    EXPECT_EQ(info.ip_address(), "10.168.1.50");
    EXPECT_EQ(info.mac_address(), "01:23:45:67:89:AB");

    delete event_list;

    cleanup_amx_bus();
}
