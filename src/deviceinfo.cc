/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <amxrt/amxrt.h>

#include <prplprobe/common.pb.h>
#include <prplprobe/deviceinfo.pb.h>
#include <prplprobe/module.pb.h>

#include <prplprobe/amx_client_utils.h>
#include <prplprobe/amx_utils.h>
#include <prplprobe/config_utils.h>
#include <prplprobe/event_loop.h>
#include <prplprobe/func_utils.h>
#include <prplprobe/grpc_utils.h>
#include <prplprobe/sahtrace_utils.h>

#include "deviceinfo.h"

#define ME "deviceinfo"

using namespace prplprobe::internal::v1;
using namespace google::protobuf;

ModuleConfiguration deviceinfo_configuration;

/* DeviceInfo */

EventList *getDeviceInfoEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("DeviceInfo.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
    }

    amxc_var_t paths;
    amxc_var_init(&paths);
    amxc_var_set_type(&paths, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, &paths, "DeviceInfo.Manufacturer");
    amxc_var_add(cstring_t, &paths, "DeviceInfo.ProductClass");
    amxc_var_add(cstring_t, &paths, "DeviceInfo.SerialNumber");
    amxc_var_add(cstring_t, &paths, "DeviceInfo.SoftwareVersion");
    amxc_var_add(cstring_t, &paths, "NetModel.Intf.ip-wan.IPAddress");
    amxc_var_add(cstring_t, &paths, "NetModel.Intf.ethLink-bridge_lan.MACAddress");

    amxc_var_t ret;
    amxc_var_init(&ret);
    int err = 0;
    err = amxb_get_multiple(bus_ctx, &paths, 1, &ret, 5);
    if (err != 0) {
        SAH_TRACEZ_ERROR(ME, "error amxb_get: %d", err);
    }

    EventList *event_list = new EventList();
    Event *event = event_list->add_event();
    event->set_kpiname("DeviceInfo");
    event->set_kpitype("");
    fill_timestamp(event);

    DeviceInfo deviceInfo;
    
    amxc_var_t *manufacturer = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "DeviceInfo.Manufacturer", AMXC_VAR_FLAG_DEFAULT))));
    amxc_var_t *product_class = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "DeviceInfo.ProductClass", AMXC_VAR_FLAG_DEFAULT))));
    string cpe_type = GET_CHAR(manufacturer, "Manufacturer") ? : "";
    cpe_type.append(" ");
    cpe_type.append(GET_CHAR(product_class, "ProductClass") ? : "");
    deviceInfo.set_cpe_type(cpe_type);

    amxc_var_t *serial_number = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "DeviceInfo.SerialNumber", AMXC_VAR_FLAG_DEFAULT))));
    deviceInfo.set_serial_number(GET_CHAR(serial_number, "SerialNumber") ? : "");

    amxc_var_t *software_version = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "DeviceInfo.SoftwareVersion", AMXC_VAR_FLAG_DEFAULT))));
    deviceInfo.set_firmware(GET_CHAR(software_version, "SoftwareVersion") ? : "");

    deviceInfo.set_device_type("HGW");

    amxc_var_t *ip = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "NetModel.Intf.ip-wan.IPAddress", AMXC_VAR_FLAG_DEFAULT))));
    deviceInfo.set_ip_address(GET_CHAR(ip, "IPAddress") ? : "");

    amxc_var_t *mac = amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(amxc_var_get_key(&ret, "NetModel.Intf.ethLink-bridge_lan.MACAddress", AMXC_VAR_FLAG_DEFAULT))));
    deviceInfo.set_mac_address(GET_CHAR(mac, "MACAddress") ? : "");

    Any *event_data = event->mutable_event_data();
    event_data->PackFrom(deviceInfo);

    amxc_var_clean(&ret);
    amxc_var_clean(&paths);

    return event_list;
}

/* END */

/* Init */

void deviceinfo_init() {
    auto client = init_grpc_client(false);

    ProbeAmxClientModule info(&client, &getDeviceInfoEventList, 3600, true);

    amxrt_el_start();
}

int deviceinfo() {
    RestoreConfiguration(deviceinfo_configuration, DEFAULT_FILE, PERSIST_FILE);
    InitSahTrace(deviceinfo_configuration.sahtrace_config());
    init_amx("deviceinfo-module", {"DeviceInfo."});
    deviceinfo_init();

    /* Cleanup */
    CleanupSahTrace();
    BackupConfiguration(deviceinfo_configuration, PERSIST_FILE);
    amxrt_stop();
    amxrt_delete();

    return 0;
}
