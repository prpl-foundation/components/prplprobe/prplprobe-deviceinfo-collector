-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/prplprobe-deviceinfo-collector
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = prplprobe-deviceinfo-collector

compile:
	$(MAKE) -C cmake all

clean:
	$(MAKE) -C cmake clean

install:
	$(INSTALL) -D -p -m 0755 $(COMPONENT) $(D)/usr/lib/prplprobe/collectors/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(D)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 config/deviceinfo-collector.cfg.default $(D)$(CONFIG_SAH_PRPLPROBE_DEVICEINFO_COLLECTOR_PERSIST_PATH)/deviceinfo-collector.cfg.default

.PHONY: compile clean install


